---
layout: page
title: Typed Self-Applicable Meta-Programming
no-sidebar: true
---

*[PhD Thesis](/public/publications/mattbrown-thesis.pdf) by Matt Brown, published 2017*

# Abstract

Self-applicable meta-programming has its roots in the early days of
computer science.
Two early examples were published in 1936: the universal Turing
machine, and a self-interpreter for the λ-calculus.
These were major advances in computability theory, but
self-application has a long history of practical uses as well.
Many languages have self-interpreters or self-hosting compilers. 
Others support self-applicable meta-programming as a general-purpose
technique that enables elegant solutions to many problems.
Until now, these techniques have been incompatible with static type
checking, which has many benefits of its own.
In this thesis I present techniques for practical self-applicable
meta-programming for statically typed languages.


# Publications
<ul>
  <li>
    <div>Typed Self-Representation in Girard's System U</div>
    <div>Matt Brown and Jens Palsberg, POPL 2015</div>
    <div><a href="https://dl.acm.org/citation.cfm?id=2676988">ACM Digital Library</a>, <a href="http://compilers.cs.ucla.edu/popl15/">Artifact Page</a></div>
  </li>
  <li>
    <div>Breaking through the Normalization Barrier: A Self-Interpreter for F-omega</div>
    <div>Matt Brown and Jens Palsberg, POPL 2016</div>
    <div><a href="https://dl.acm.org/citation.cfm?id=2837623">ACM Digital Library</a>, <a href="http://compilers.cs.ucla.edu/popl16/">Artifact Page</a></div>
  </li>
  <li>
    <div>Typed Self-Evaluation via Intensional Type Functions</div>
    <div>Matt Brown and Jens Palsberg, POPL 2017</div>
    <div><a href="https://dl.acm.org/citation.cfm?id=3009853">ACM Digital Library</a>, <a href="http://compilers.cs.ucla.edu/popl17/">Artifact Page</a></div>
  </li>
  <li>
    <div>Jones-Optimal Partial Evaluation by Specialization-Safe Normalization</div>
    <div>Matt Brown and Jens Palsberg, POPL 2018</div>
    <div><a href="http://compilers.cs.ucla.edu/popl18/">Artifact Page</a></div>
  </li>

</ul>





