---
layout: paper
date: 2022-05-29
title: Striking a Balance- Pruning False-Positives from Static Call Graphs
paper: /papers/balancing-callgraphs.pdf
conference: ICSE ’22, May 21–29, 2022, Pittsburgh, PA, USA
artifact: https://doi.org/10.5281/zenodo.5177161
authors:
  - Akshay Utture
  - Shuyang Liu
  - Christian Gram Kalhauge
  - Jens Palsberg

---
Researchers have reported that static analysis tools rarely achieve a false-positive rate that would make them attractive to developers. We overcome this problem by a technique that leads to reporting fewer bugs but also much fewer false positives. Our technique prunes the static call graph that sits at the core of many static analyses. Specifically, static call-graph construction proceeds as usual, after which a call-graph pruner removes many false-positive edges but few true edges. The challenge is to strike a balance between being aggressive in removing false-positive edges but not so aggressive that no true edges remain. We achieve this goal by automatically producing a call-graph pruner through an automatic, ahead-of-time learning process. We added such a call-graph pruner to a software tool for null-pointer analysis and found that the false-positive rate decreased from 73% to 23%. This improvement makes the tool more useful to developers.


<!--end-abstract-->
