---
layout: paper
title: Automatic Atomicity Verification for Clients of Concurrent Data Structures
authors: 
    - Mohsen Lesani
    - Todd Millstein
    - Jens Palsberg
date: 2014-07-18
conference: CAV 2014, July 18-22, 2014, Vienna, Austria 
paper: http://web.cs.ucla.edu/~palsberg/paper/cav14.pdf
---

Mainstream programming languages offer libraries of concurrent data structures.
Each method call on a concurrent data structure appears to take effect
atomically. However, clients of such data structures often require stronger
guarantees. We have developed an automatic and modular verification technique
for clients of concurrent data structures along with a novel sufficient
condition for atomicity of clients called condensability and constructed a tool
called Snowflake that generates proof obligations for condensability of Java
client methods.

<!--end-abstract-->
