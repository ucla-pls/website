---
layout: paper
title: Logical Bytecode Reduction
authors: 
    - Christian Gram Kalhauge
    - Jens Palsberg
date: 2021-06-21
conference: PLDI 2021, June 20-26, 2021, Virtual Conference 
paper: http://web.cs.ucla.edu/~palsberg/paper/pldi21-reduction.pdf 

---
Reducing a failure-inducing input to a smaller one is chal- lenging for input with internal dependencies because most sub-inputs are invalid. Kalhauge and Palsberg made progress on this problem by mapping the task to a reduction prob- lem for dependency graphs that avoids invalid inputs en- tirely. Their tool J-Reduce efficiently reduces Java bytecode to 24% of its original size, which made it the most effective tool until now. However, the output from their tool is of- ten too large to be helpful in a bug report. In this paper, we show that more fine-grained modeling of dependencies leads to much more reduction. Specifically, we use proposi- tional logic for specifying dependencies and we show how this works for Java bytecode. Once we have a propositional formula that specifies all valid sub-inputs, we run an algo- rithm that finds a small, valid, failure-inducing input. Our algorithm interleaves runs of the buggy program and calls to a procedure that finds a minimal satisfying assignment. Our experiments show that we can reduce Java bytecode to 4.6% of its original size, which is 5.3 times better than the 24.3% achieved by J-Reduce. The much smaller output is more suitable for bug reports.

<!--end-abstract-->
