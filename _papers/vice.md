---
layout: paper
date: 2013-07-22
title: Testing versus Static Analysis of Maximum Stack Size
paper: http://web.cs.ucla.edu/~palsberg/paper/compsac13.pdf
authors:
  - Mahdi Eslamimehr
  - Jens Palsberg
conference: COMPSAC'13, July 22–26, 2013, Kyoto, Japan
---

For event-driven software on resource-constrained devices, estimates of the maximum stack size can be of paramount importance. For example, a poor estimate led to software failure and closure of a German railway station in 1995. Static analysis may produce a safe estimate but how good is it? In this paper we use testing to evaluate the state-of-the-art static analysis of maximum stack size for event-driven assembly code. First we note that the state-of-the-art testing approach achieves a maximum stack size that is only 67 percent of that achieved by static analysis. Then we present better testing approaches and use them to demonstrate that the static analysis is near optimal for our benchmarks. Our first testing approach achieves a maximum stack size that on average is within 99 percent of that achieved by static analysis, while our second approach achieves 94 percent and is two orders of magnitude faster. Our results show that the state-of-the-art static analysis produces excellent estimates of maximum stack size.

<!--end-abstract-->
