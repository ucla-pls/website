---
layout: paper
title: Race Directed Scheduling of Concurrent Programs
authors: 
    - Mahdi Eslamimehr
    - Jens Palsberg
conference: PPoPP ’14, February 15–19, 2014, Orlando, Florida, USA
paper: http://web.cs.ucla.edu/~palsberg/paper/ppopp14.pdf
date: 2014-02-15
---

Detection of data races in Java programs remains a difficult problem.
The best static techniques produce many false positives, and also the
best dynamic techniques leave room for improvement. With Racageddon we
present a new technique called "Race Directed Scheduling" that for
a given race candidate searches for an input and a schedule that lead to
the race. The search iterates a combination of concolic execution and
schedule improvement, and turns out to find useful inputs and schedules
efficiently.

<!--end-abstract-->
