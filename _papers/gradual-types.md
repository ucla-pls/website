---
layout: paper
date: 2020-01-22
title: What is Decidable about Gradual Types?
authors:
  - Zeina Migeed
  - Jens Palsberg
conference: POPL 2020, January 22–25, 2020, New Orleans, Louisiana, USA
paper: /papers/What_is_Decidable_about_Gradual_Types___with_appendix_.pdf
artifact: https://github.com/migeed-z/Maximal-Migration
\cite-as: |
   Zeina Migeed and Jens Palsberg. 2019. What is decidable about gradual types? Proc. ACM Program. Lang. 4, POPL, Article 29 (December 2019), 29 pages. DOI:https://doi.org/10.1145/3371097

---
Programmers can use gradual types to migrate programs to have more precise type annotations and thereby improve their readability, efficiency, and safety. Such migration requires an exploration of the migration space and can benefit from tool support, as shown in previous work. Our goal is to provide a foundation for better tool support by settling decidability questions about migration with gradual types. We present three algorithms and a hardness result for deciding key properties and we explain how they can be useful during an exploration. In particular, we show how to decide whether the migration space is finite, whether it has a top element, and whether it is a singleton. We also show that deciding whether it has a maximal element is NP-hard. Our implementation of our algorithms worked as expected on a suite of microbenchmarks.

<!--end-abstract-->

