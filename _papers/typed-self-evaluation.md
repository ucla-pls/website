---
layout: paper
title: Typed self-evaluation via intensional type functions
authors: 
    - Matt Brown
    - Jens Palsberg
conference: POPL'17, January 15-21, 2017, Paris, France
date: 2017-01-15
paper: https://dl.acm.org/citation.cfm?id=3009853
aritfact: http://compilers.cs.ucla.edu/popl17/
---

Many popular languages have a self-interpreter, that is, an interpreter for the language written in itself. So far, work on polymorphically-typed self-interpreters has concentrated on self-recognizers that merely recover a program from its representation. A larger and until now unsolved challenge is to implement a polymorphically-typed self-evaluator that evaluates the represented program and produces a representation of the result. We present Fωμi, the first λ-calculus that supports a polymorphically-typed self-evaluator. Our calculus extends Fω with recursive types and intensional type functions and has decidable type checking. Our key innovation is a novel implementation of type equality proofs that enables us to define a versatile representation of programs. Our results establish a new category of languages that can support polymorphically-typed self-evaluators.

<!--end-abstract-->
