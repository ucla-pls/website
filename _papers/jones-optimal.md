---
layout: paper
title: Jones-Optimal Partial Evaluation by Specialization-Safe Normalization
authors:
    - Matt Brown
    - Jens Palsberg
paper: http://compilers.cs.ucla.edu/popl18/popl18-full.pdf
artifact: http://compilers.cs.ucla.edu/popl18/
conference: POPL'18, January 7-13, 2018, Los Angeles, California, USA
date: 2018-01-07
---

We present partial evaluation by specialization-safe normalization,
a novel partial evaluation technique that is Jones-optimal, that can be
self-applied to achieve the Futamura projections and that can be
type-checked to ensure it always generates code with the correct type.
Jones-optimality is the gold-standard for nontrivial partial evaluation
and guarantees that a specializer can remove an entire layer of
interpretation. We achieve Jones-optimality by using a novel
affine-variable static analysis that directs specialization-safe
normalization to always decrease a program’s runtime.

We demonstrate the robustness of our approach by showing
Jones-optimality in a variety of settings. We have formally proved that
our partial evaluator is Jones-optimal for call-by-value reduction, and
we have experimentally shown that it is Jones-optimal for call-by-value,
normal-order, and memoized normal-order. Each of our experiments tests
Jones-optimality with three different self-interpreters.

We implemented our partial evaluator in F$_\omega^{\mu i}$, a recent
language for typed self-applicable meta-programming. It is the first
Jones-optimal and self-applicable partial evaluator whose type
guarantees that it always generates type-correct code.

<!--end-abstract-->
