---
layout: paper
title: Scalable Deadlock Detection for Concurrent Programs
date: 2014-11-16
authors: 
    - Mahdi Eslamimehr
    - Jens Palsberg
conference: FSE’14, November 16–22, 2014, Hong Kong, China
paper: http://web.cs.ucla.edu/~palsberg/paper/fse14.pdf
---

We present a new technique to find real deadlocks in concurrent
programs. For 4.5 million lines of Java, our technique found almost
twice as many real deadlocks as four previous techniques combined. Among
those, 33 deadlocks that happened after more than one million
computation steps, including 27 new deadlocks. We first use a known
technique to find 1275 deadlock candidates and then we determine that
146 of them are real deadlocks. Our technique combines previous work on
concolic execution with a new constraint-based approach that iteratively
drives an execution towards a deadlock candidate.

<!--end-abstract-->
