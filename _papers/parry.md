---
layout: paper
date: 2015-10-23
title: Declarative Fence Insertion
paper: http://web.cs.ucla.edu/~palsberg/paper/oopsla15.pdf
project: https://bitbucket.org/ucla-pls/parry/src/master/
authors:
  - John Bender
  - Jens Palsberg
conference: OOPSLA '15, Oktober 23-30, 2015, Pittsburgh PA, USA
---

Previous work has shown how to insert fences that enforce sequential
consistency. However, for many concurrent algorithms, sequential consistency is
unnecessarily strong and can lead to high execution overhead. The reason is
that, often, correctness relies on the execution order of a few specific pairs
of instructions. Algorithm designers can declare those execution orders and
thereby enable memory-model- independent reasoning about correctness and also
ease implementation of algorithms on multiple platforms. We present a
declarative approach to specify and enforce execution orders. Our fence
insertion algorithm first identifies the execution orders that a given memory
model enforces automatically, and then inserts fences that enforce the rest. For
our benchmark experiments with the x86 and ARMv7 memory models our tool inserts
fences that are competitive with those inserted by the original authors. Our
tool is the first to insert fences into transactional memory algorithms and it
solves the long-standing problem of how to easily port such algorithms to a
novel memory model.

<!--end-abstract-->
