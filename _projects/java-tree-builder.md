---
layout: page
title: Java Tree Builder
homepage: http://compilers.cs.ucla.edu/jtb/
---

*A frontend for The Java Compiler Compiler.*

JTB is a syntax tree builder to be used with the Java Compiler Compiler
(JavaCC) parser generator.  It takes a plain JavaCC grammar file as
input and automatically generates a set of syntax tree classes, visitor
interfaces, and a Java CC grammar.

<!--end-abstract-->
