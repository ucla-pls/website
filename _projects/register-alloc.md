---
title: Register Alloc by Puzzle Solving
homepage: http://compilers.cs.ucla.edu/fernando/projects/puzzles/
---

*A new model for register allocation.*

This project consists in developing a new model for register allocation that has fast compilation time, produces code of good quality, is able to address the many irregularities found in common computer architectures and is intuitive enough to be easily understood. We have shown that register allocation can be viewed as solving a collection of puzzles. We model the register file as a puzzle board and the program variables as puzzle pieces; pre-coloring and register aliasing fit in naturally.

<!--end-abstract-->
