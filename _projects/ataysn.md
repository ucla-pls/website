---
layout: page
title: Ataysn
homepage: http://compilers.cs.ucla.edu/atasyn/
---

*Timing analysis of TCP servers for surviving denial-of-service attacks.*

Denial-of-service attacks are becoming more frequent and sophisticated. Researchers have proposed a variety of defenses but can we validate a server implementation in a systematic manner? Atasyn focuses on a particular attack, SYN flooding, where an attacker sends many TCP-connection requests to a victim's machine.  It automatically transforms a TCP-server implementation into a timed automaton, and it transforms an attacker model, given by the output of a packet generator, into another timed automaton.

<!--end-abstract-->
