---
layout: page
title: Typed Self-Applicable Meta-Programming
homepage: /tsamp
---

*Type-correctness by type-checking*

This work brings the benefits of static typechecking to
self-interpreters, self-applicable partial evaluators, and other kinds
of self-applicable metaprograms. A key result is that type-correctness
properties -- that a self-interpreter preserves the type of the input
program, that a program transformation changes the type in the expected
way, etc -- are guaranteed simply by type-checking.

<!--end-abstract-->
