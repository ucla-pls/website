---
layout: page
title: Transactional Memory Algorithms
---

*On the Correctness of Transactional Memory Algorithms*

Transactional Memory (TM) provides programmers with a high-level and
composable concurrency control abstraction. The correct execution of
client programs using TM is directly dependent on the correctness of the
TM algorithms. In return for the simpler programming model, designing
a correct TM algorithm is an art. We have introduced a language for
architecture-independent specification of synchronization algorithms
that assists in the design of correct TM algorithms along with a new
correctness condition, markability, and a sound program logic called
synchronization object logic (SOL) that supports reasoning about the
execution order and linearization orders of method calls.

<!--end-abstract-->
