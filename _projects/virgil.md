---
layout: page
title: Virgil
homepage: http://compilers.cs.ucla.edu/virgil/
---

*Objects on the head of a pin.*

Virgil is a light-weight object-oriented programming language that is
designed for building software for resource-constrained embedded systems
at the lowest level. Microcontroller programmers can now develop
complete software systems, including hardware drivers and OS services,
in one language, without the need to resort to unsafe libraries or
native code. Virgil also provides a whole-program compiler system that
applies novel global optimization techniques to produce efficient
machine code that runs directly on the hardware, without the need of
a virtual machine or a language runtime system.

<!--end-abstract-->
