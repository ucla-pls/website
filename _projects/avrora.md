---
layout: page
title: Avrora 
homepage: http://compilers.cs.ucla.edu/avrora/
---

*Scalable sensor network simulation with precise timing.*

Avrora is a set of simulation and analysis tools for programs written
for the AVR microcontroller produced by Atmel and the Mica2 sensor
nodes. Avrora contains a flexible framework for simulating and analyzing
assembly programs, providing a clean Java API and infrastructure for
experimentation, profiling, and analysis.

<!--end-abstract-->
