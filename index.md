---
layout: default
title: Home
paper_cutoff: 2017-01-01 00:00:01
---

# Welcome!

<img class="ucla-logo" src="/public/images/ucla-logo.jpg" alt="UCLA logo"/>
Welcome to the website for the UCLA Compilers Group supervised by Jens Palsberg. Here you'll find information about current and former [students](/people#students), [researchers](/people#researchers), [projects](/projects) and [funding](/funding). Included below is a small selection of recent research projects from the group.

## Recent Papers

{% include papers-list.md paper_cutoff=page.paper_cutoff %}

## Current Projects

{% include projects-list.md all=false %}

{% if paginator.posts.length %}
<div class="pagination">
  {% if paginator.next_page %}
    <a class="pagination-item older" href="/page{{paginator.next_page}}">Older</a>
  {% else %}
    <span class="pagination-item older">Older</span>
  {% endif %}
  {% if paginator.previous_page %}
    {% if paginator.page == 2 %}
      <a class="pagination-item newer" href="/">Newer</a>
    {% else %}
      <a class="pagination-item newer" href="/page{{paginator.previous_page}}">Newer</a>
    {% endif %}
  {% else %}
    <span class="pagination-item newer">Newer</span>
  {% endif %}
</div>
{% endif %}
