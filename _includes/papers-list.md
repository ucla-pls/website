<ul>
{% for paper in site.papers reversed %}
    {%unless include.paper_cutoff and paper.date < include.paper_cutoff %} 
      <li>{% include paper-ref.md paper=paper %} </li>
    {% endunless %}
{% endfor %}
</ul>
