<a href="{{ include.paper.url }}">{{ include.paper.title }}</a>,
<small>
{{ include.paper.authors | array_to_sentence_string }},
{{ include.paper.conference }}
</small>
