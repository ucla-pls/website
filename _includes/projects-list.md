<div class="projects">
  {% for project in site.projects %}
    {% if include.all or project.current %}
      <div class="project">
        <h2 class="project-title">
        {% if project.homepage %}
        <a href="{{project.homepage}}">{{ project.title}}</a>
        {% else %}
        <a href="{{project.url}}">{{ project.title}}</a>
        {% endif %}
        </h2>

        {{ project.excerpt }}
      </div>
    {% endif %}
  {% endfor %}
</div>
