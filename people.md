---
layout: page
title: People
sidebar: true
---

## Faculty

![Jens Palsberg](/public/images/jens-palsberg.jpg) [Jens Palsberg](http://www.cs.ucla.edu/~palsberg/) is Professor and Chair of Computer Science at UCLA, University of California, Los Angeles. His research interests span the areas of compilers, embedded systems, programming languages, software engineering, and information security. He is the editor-in-chief of ACM Transactions of Programming Languages and Systems, a member of the editorial board of Information and Computation, a former member of the editorial board of IEEE Transactions on Software Engineering, and a former conference program chair of POPL, TACAS, SAS, EMSOFT, MEMOCODE, PASTE, SREIS. In 2012 he received the ACM SIGPLAN Distinguished Service Award.

<a name="students"></a>
## Current PhD Students

![Zeina Migeed](/public/images/zeina-migeed.jpg) [Zeina Migeed](https://zeinamigeed.com) is a fifth year PhD student. 
She studies computability and complexity aspects of gradually typed systems. 
She is currently studying the computability of making gradually typed programs as static as possible.

![Shuyang Liu](/public/images/shuyang-liu.jpg) [Shuyang Liu](https://shuyanglearningnotes.wordpress.com) is a fourth year PhD student. 
She is currently working on formalizing and proving important properties of the relaxed memory model for Java. 
She previously has worked on various topics including applying augmented reality technologies to post-stroke rehabilitation and return-oriented programming. 

![Micky Abir](/public/images/micky-abir.jpeg)[Micky Abir](https://mickyabir.com)
is a second year PhD student. He currently works on optimization of quantum programs.
He has previously worked on the LLVM backend of the K Framework, as well as formalizing 
the continuation-style semantics of the IMP + Lists language and its reachability logic-based prover during his master's at UIUC.

## Graduated PhD Students

![Akshay Utture](/public/images/akshay-utture.jpg) [Akshay Utture](https://akshayutture.github.io) 
received his PhD in 2023 and is now working at Uber's Programming Systems group. 
He worked on improving the precision, scalability, and repair suggestions for Java Static Analysis tools. 
He previously worked on the runtime
design of the [X10](http://x10-lang.org) task-parallel programming language
during his integrated Bachelors+Masters at IIT Madras in India.

![Christian Kalhauge](/public/images/christian-kalhauge.jpg) [Christian
Kalhauge](http://kalhauge.dk/christian) received his PhD in
2020, and is now a postdoc at DTU.
His primary focus is dynamic and static
analysis of programs. He worked on the [ONR
TPCP](http://debloating.cs.ucla.edu/) and the [NJR](/njr) project.
He has a Masters of Science in Engineering from DTU in Denmark, with a focus on
reliable software systems.

![John Bender](/public/images/john-bender.jpg) [John Bender](http://johnbender.us) received
his PhD in 2019. He works on compilers and verification
techniques for concurrent algorithms executing on weak memory models.
He previously worked in industry as an open source software
developer on projects like [vagrant](http://vagrantup.com)
and [jquery](http://jquery.com). 

![Matt Brown](/public/images/matt-brown.jpg) Matt Brown received his PhD in
2017, and is now at [Intentionet](http://www.intentionet.com). His PhD work
on [Typed Self-Applicable Meta-Programming](/tsamp) used typed program
representation techniques to ensure correctness properties of self-applicable
meta-programs like self-interpreters. Other research interests include
functional programming, type systems, concurrency and program verification.

![Mahdi Eslamimehr](/public/images/mahdi-eslamimehr.jpg) [Mahdi Eslamimehr](http://www.mah-d.com) was
born in Tehran, Iran. He received his bachelor in computer engineering from
Sharif University, and his master in computer science from Linkoping University.
He received his PhD in 2014 from UCLA. His research interest lies in the
intersection of software testing, compiler construction and programming
languages.

![Mohsen Lesani](/public/images/mohsen-lesani.jpg) [Mohsen Lesani](http://www.cs.ucr.edu/~lesani/) 
has research experience with IBM Research, Oracle (Sun) Labs, HP Labs and EPFL and is 
currently an Associate Professor at University of California Riverside. 
His research focuses on the design, implementation, testing and verification of safety and security of concurrent and distributed systems. 

**Previously**:

Jonathan Lee, Fernando Pereira, Krishna Nandivada, Christian Grothoff, Ben Titzer

## Former Graduate Students

Kevin Chang, Vidyut Samanta, Joseph Cox

## Former Undergraduate Students

Lorenz Verzosa, Adam Harmetz

## Past Visitors

Daniel Lee, Keith Mayoral, Ryan Leon
