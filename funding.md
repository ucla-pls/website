---
layout: page
title: Funding
sidebar: true
---

## [NSF](http://www.nsf.gov/)

The National Science Foundation (NSF) is an independent federal agency created by Congress in 1950 "to promote the progress of science; to advance the national health, prosperity, and welfare; to secure the national defense…" With an annual budget of $7.2 billion (FY 2014), we are the funding source for approximately 24 percent of all federally supported basic research conducted by America's colleges and universities. In many fields such as mathematics, computer science and the social sciences, NSF is the major source of federal backing.

## [DARPA](http://www.darpa.mil)

Creating breakthrough technologies for national security is the mission of the Defense Advanced Research Projects Agency (DARPA). By making pivotal investments in new technology-driven ideas for the United States, DARPA imagines and makes possible new capabilities for overcoming the multifaceted threats and challenges that lie ahead. This makes a better, more secure future possible. In an uncertain world, with constrained budgets, providing these new capabilities is more important than ever.
