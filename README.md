## UCLA Compiler Group Website

This is the compilers group website. It includes information about the people, projects, and funding of the graduate students and other participants working with Jens Palsberg.

### Construction

The site is built from [markdown](http://en.wikipedia.org/wiki/Markdown) with a static site generator called [jekyll](http://jekyllrb.com/). The goal is to make it incredible easy and painless for anyone to contribute so that the site stays up to date.

The high-level idea is that all changes to the website are made in this repository.
Since everything is in markdown, it is easier to make changes and we can only focus on the main
content, without worrying about layout, site-map, etc.
Once the changes are made in markdown, a static-site HTML is generated with the
jekyll command and added to the
_site folder, which is a git submodule linked to  [https://bitbucket.org/ucla-pls/ucla-pls.bitbucket.org](https://bitbucket.org/ucla-pls/ucla-pls.bitbucket.org)
Then one can run a 'git commit' on the ucla-pls.bitbucket.org repository to publish the website. 

### Setup

First remember to download all the submodules:

```
$ git submodule init
$ git submodule update
```

If you're on a Mac you already have ruby installed, so to set up jekyll all you need is:

```
$ sudo gem install jekyll -v 3.8.6
$ sudo gem install redcarpet -v 3.4.0
```

On Linux you'll have to install Ruby and RubyGems using your package manager and then run the same command.

### Contributing

When you're ready to work on the site you can start the jekyll generator with:

```
$ cd path/to/project/root
$ jekyll serve
```
Use '--serve' instead of 'serve' if running with an older version of Jekyll.

This will start both a webserver so you can [view](http://localhost:4000) the site at `http://localhost:4000` and a watcher that tracks your changes and rebuilds the static HTML accordingly.

Then you can edit the following pages at the root of the project:

* _posts/2014-04-01-welcome.md (About page)
* people.md
* project.md
* funding.md

Please refer to the [markdown documentation](http://daringfireball.net/projects/markdown/syntax) for more information on the formatting and how it translates to HTML.

### Publishing
To publish your changes to the compilers.cs.ucla.edu site, navigate to
the `_site/` folder and commit and push the results to the 
[https://bitbucket.org/ucla-pls/ucla-pls.bitbucket.org](https://bitbucket.org/ucla-pls/ucla-pls.bitbucket.org). Make sure to point the `_sites/` submodule to the new commit.

Use the following command to do the push
```
git push origin HEAD:<name-of-remote-branch>
```


There is a script which will automatically update the website to reflect the contents of the 'ucla-pls/ucla-pls.bitbucket.org' repository every 15 minutes 

### Troubleshooting

1) If 'git submodule update' fails, try this solution 
(https://stackoverflow.com/questions/8197089/fatal-error-when-updating-submodule-using-git) 
